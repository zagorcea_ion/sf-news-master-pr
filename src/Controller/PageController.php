<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\NewsRepository;
use App\Utils\Slugger;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * Show home page
     * @Route("/", name="home_page")
     * @param NewsRepository $repository
     * @return Response
     */
    public function homePage(NewsRepository $repository)
    {
        $data = [
            'must_read'    => $repository->getIsMustRead(),
            'news'         => $repository->getLast(),
            'most_popular' => $repository->getMostPopular(),
        ];

        return $this->render('page/home.html.twig', $data);
    }

    /**
     * Show news page
     * @Route("/news", name="news_page")
     * @param Request $request
     * @param NewsRepository $repository
     * @param PaginatorInterface $paginator
     * @param null $category
     * @return Response
     */
    public function newsPage(Request $request, NewsRepository $repository, PaginatorInterface $paginator, $category = null)
    {
        if (!is_null($category)) {
            $idCategory = Slugger::getIdUrl($category);
            $news = $repository->getLast($idCategory);

        } else  {
            $news = $repository->getLast();
        }

        $news = $paginator->paginate(
            $news,                                             //query
            $request->query->getInt('page', 1),   //page number
            10                                           //per page
        );

        $data = [
            'news'         => $news,
            'most_popular' => $repository->getMostPopular(),
        ];

        return $this->render('page/news.html.twig', $data);
    }

    /**
     * Show news details page
     * @Route("/news/{slug}", name="news_details_page")
     * @param NewsRepository $repository
     * @param string $slug
     * @return Response
     */
    public function newsDetailsPage(NewsRepository $repository, string $slug)
    {
        $idNews = Slugger::getIdUrl($slug);

        $data = [
            'record'             => $repository->findOneBy(['id' => $idNews]),
            'most_popular'       => $repository->getMostPopular(),
            'can_be_interesting' => $repository->getRandom(3, $idNews)
        ];

//        dump($data['can_be_interesting']);die;
        return $this->render('page/news_detail.html.twig', $data);
    }
    /**
     * Show public contact page
     * @Route("/contact", name="contact_page")
     * @return Response
     */
    public function contactPage()
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact, [
            'action' => $this->generateUrl('contact_store'),
            'method' => 'POST'
        ]);

        return $this->render('page/contact.html.twig', ['form' => $form->createView()]);
    }
}
