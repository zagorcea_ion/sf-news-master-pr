<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Utils\FilesHelper;
use App\Utils\Slugger;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class NewsController extends AbstractController
{
    /**
     * Show records list
     * @Route("/admin/news", name="news_index")
     * @return Response
     */
    public function index()
    {
        return $this->render('admin/news/index.html.twig', [
            'activePage' => 'news'
        ]);
    }

    /**
     * Return JSON with news list for data table plugin
     * @Route("/admin/news-list", name="news_list")
     * @param Request $request
     * @param NewsRepository $repository
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function newsList(Request $request, NewsRepository $repository)
    {
        $columns = [
            0 => 'title',
            5 => 'created_at',
        ];

        //Config search data
        $search = $request->get('search');

        if (!empty($search['value'])) {
            $params['search'] = $search['value'];
        }

        //Config order
        $orderConfig = $request->get('order');
        $params['order_column'] = $columns[$orderConfig[0]['column']];
        $params['order_dir'] = $orderConfig[0]['dir'];

        //Config pagination
        $params['limit'] = $request->get('length');
        $params['offset'] = $request->get('start');

        $news = $repository->getDtData($params);

        $data = [];

        if (!empty($news)) {
            foreach ($news as $key => $item) {
                $serializer = new Serializer(array(new DateTimeNormalizer()));
                $dateAddAsString = $serializer->normalize($item->getCreatedAt());

                $data[$key] = [
                    'title'        => $item->getTitle(),
                    'category'     => $item->getIdCategory()->getName(),
                    'image'        => $this->renderView('admin/news/partial/image.html.twig', ['image' => $item->getImage()]),
                    'is_published' => $this->renderView('admin/news/partial/is_published.html.twig', ['id' => $item->getId(), 'isPublished' => $item->getIsPublished()]),
                    'is_must_read' => $this->renderView('admin/news/partial/is_must_read.html.twig', ['id' => $item->getId(), 'isMustRead' => $item->getIsMustRead()]),
                    'date_add'     => date('d.m.Y', strtotime($dateAddAsString)),
                    'action'       => $this->renderView('admin/news/partial/action.html.twig', ['id' => $item->getId()])
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $repository->countAll();
        $output['recordsFiltered'] = $repository->countAll($params);

        $response = new JsonResponse($output);

        return $response;
    }

    /**
     * Create entity
     * @Route("/admin/news/create", name="news_create")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $news = new News();

        $form = $this->createForm(NewsType::class, $news, [
            'action' => '',
            'method' => 'POST'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $request->files->get('news')['image'];

            $news->setCreatedAt(new \DateTime('@'.strtotime('now')));
            $news->setDatePublish(new \DateTime('@'.strtotime($request->get('news')['date_publish'])));
            $news->setAlias(Slugger::slugify($news->getTitle()));

            if (!empty($image)) {
                $news->setImage(FilesHelper::saveFile($this->getParameter('news_file_dir'), $image));
            }

            $em->persist($news);
            $em->flush();

            $this->addFlash('success', 'News was added successfully');

            return $this->redirectToRoute('news_index');
        }

        return $this->render('admin/news/form.html.twig', ['activePage' => 'news', 'form' => $form->createView()]);
    }

    /**
     * Update entity
     * @Route("/admin/news/update/{news}", name="news_update")
     * @param Request $request
     * @param News $news
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, News $news)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(NewsType::class, $news, [
            'action' => '',
            'method' => 'POST'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $request->files->get('news')['image'];

            $news->setAlias(Slugger::slugify($news->getTitle()));

            if (!empty($image)) {
                if (!is_null($news->getImage())) {
                    FilesHelper::deleteFile($this->getParameter('news_file_dir'), $news->getImage());
                }

                $news->setImage(FilesHelper::saveFile($this->getParameter('news_file_dir'), $image));
            }

            $em->persist($news);
            $em->flush();

            $this->addFlash('success', 'News was added updated');

            return $this->redirectToRoute('news_index');
        }

        return $this->render('admin/news/form.html.twig', ['activePage' => 'news', 'form' => $form->createView(), 'news' => $news]);
    }

    /**
     * Delete record
     * @Route("/admin/news/delete/{news}", name="news_delete")
     * @param News $news
     * @return JsonResponse
     */
    public function delete(News $news)
    {
        FilesHelper::deleteFile($this->getParameter('news_file_dir'), $news->getImage());

        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => "News was delete successfully"
        ]);
    }

    /**
     * Make news published
     * @Route("/admin/news/is-published/{news}", name="news_is_published")
     * @param News $news
     * @return JsonResponse
     */
    public function isPublished(News $news)
    {
        $em = $this->getDoctrine()->getManager();

        if ($news->getIsPublished()) {
            $news->setIsPublished(0);
            $message = 'News was unpublished successfully';

        } else {
            $news->setIsPublished(1);
            $message = 'News was published successfully';
        }

        $em->persist($news);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => $message
        ]);
    }

    /**
     * Make news must read
     * @Route("/admin/news/is-must-read/{news}", name="news_is_must_read")
     * @param News $news
     * @return JsonResponse
     */
    public function isMustRead(News $news)
    {
        $em = $this->getDoctrine()->getManager();

        if ($news->getIsMustRead()) {
            $news->setIsMustRead(0);
            $message = 'News is not "must read"';

        } else {
            $news->setIsMustRead(1);
            $message = 'News was made "must read"';
        }

        $em->persist($news);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => $message
        ]);
    }
}
