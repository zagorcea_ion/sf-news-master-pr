<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Utils\FilesHelper;
use App\Form\UserProfileType;
use App\Form\UserChangePasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils as AuthenticationUtilsAlias;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtilsAlias $utils
     * @return Response
     */
    public function login(AuthenticationUtilsAlias $utils)
    {
        $error = $utils->getLastAuthenticationError();
        return $this->render('admin/user/login.html.twig', ['error' => $error]);
    }

    
    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {

    }
    
    /**
     * @Route("/admin/user/profile/{user}", name="user_profile")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function profile(Request $request, User $user)
    {
        $profile = $this->createForm(UserProfileType::class, $user, [
            'action' => $this->generateUrl('user_profile_update', ['user' => $user->getId()]),
            'method' => 'POST'
        ]);
        $profile->handleRequest($request);

        $changePassword = $this->createForm(UserChangePasswordType::class, $user , [
            'action' => $this->generateUrl('user_change_password', ['user' => $user->getId()]),
            'method' => 'POST'
        ]);

        return $this->render('admin/user/profile.html.twig', [
            'activePage' => 'profile',
            'user'           => $user,
            'profile'        => $profile->createView(),
            'changePassword' => $changePassword->createView()
        ]);
    }

    /**
     * Update profile
     * @Route("/admin/user/profile/update/{user}", name="user_profile_update")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $oldAvatar = $user->getAvatar();

        $form = $this->createForm(UserProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($user->getAvatar() != $oldAvatar) {
                if (!is_null($oldAvatar)) {
                    FilesHelper::deleteFile($this->getParameter('users_file_dir'), $oldAvatar);
                }

                $user->setAvatar(FilesHelper::saveBase64Image($this->getParameter('users_file_dir'), $user->getAvatar()));
            }

            $em->persist($user);
            $em->flush();
        }

        return new JsonResponse([
            'message' => "Profile was updated"
        ]);
    }

    /**
     * User change password
     * @Route("/admin/user/profile/chage-password/{user}", name="user_change_password")
     * @param Request $request
     * @param User $user
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function changePassword(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$encoder->isPasswordValid($user, $request->get('user_change_password')['old_password'])) {
            return new JsonResponse([
                'status'  => "error",
                'message' => "Old password is incorect"
            ]);

        } else {
            $user->setPassword($encoder->encodePassword($user, $request->get('user_change_password')['password']));

            $em->persist($user);
            $em->flush();

            return new JsonResponse([
                'status'  => "success",
                'message' => "Password was successfully changed"
            ]);
        }
    }
}
