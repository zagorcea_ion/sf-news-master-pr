<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

class ContactController extends AbstractController
{

    /**
     * Show contact list
     * @Route("/admin/contact", name="contact_index")
     */
    public function index()
    {
        return $this->render('admin/contact/index.html.twig', ['activePage' => 'contact']);
    }

    /**
     * Return JSON with categories list for data table plugin
     * @Route("/admin/contact-list", name="contact_list")
     * @param Request $request
     * @param ContactRepository $repository
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws ExceptionInterface
     */
    public function contactList(Request $request, ContactRepository $repository)
    {
        $columns = [
            5 => 'created_at',
        ];

        //Config order
        $orderConfig = $request->get('order');
        $params['order_column'] = $columns[$orderConfig[0]['column']];
        $params['order_dir'] = $orderConfig[0]['dir'];

        //Config pagination
        $params['limit'] = $request->get('length');
        $params['offset'] = $request->get('start');

        $contact = $repository->getDtData($params);

        $data = [];

        if (!empty($contact)) {
            foreach ($contact as $key => $item) {
                $serializer = new Serializer(array(new DateTimeNormalizer()));
                $dateAsString = $serializer->normalize($item->getCreatedAt());

                $data[$key] = [

                    'name'     => $item->getName(),
                    'email'    => $item->getEmail(),
                    'phone'    => $item->getPhone(),
                    'website'  => $item->getWebsite(),
                    'message'  => $item->getMessage(),
                    'date_add' => date('d.m.Y', strtotime($dateAsString)),
                    'viewed'   => $this->renderView('admin/contact/partial/viewed.html.twig', ['id' => $item->getId(), 'viewed' => $item->getViewed()]),
                    'action'   => $this->renderView('admin/contact/partial/action.html.twig', ['id' => $item->getId()])
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $repository->countAll();
        $output['recordsFiltered'] = count($contact);

        $response = new JsonResponse($output);

        return $response;
    }

    /**
     * Store contact
     * @Route("/contact/store", name="contact_store")
     * @param Request $request
     * @throws Exception
     * @return JsonResponse
     */
    public function storeContact(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setCreatedAt(new \DateTime('@'.strtotime('now')));

            $em->persist($contact);
            $em->flush();
        }

        return new JsonResponse([
            'status'  => 'success',
            'message' => "Contact message was added"
        ]);

    }

    /**
     * Make contact is viewed
     * @Route("/admin/contact/is-viewed/{contact}", name="contact_is_viewed")
     * @param Contact $contact
     * @return JsonResponse
     */
    public function isViewed(Contact $contact)
    {
        $em = $this->getDoctrine()->getManager();

        if ($contact->getViewed()) {
            $contact->setViewed(0);
            $message = 'Contact is not "viewed"';

        } else {
            $contact->setViewed(1);
            $message = 'Contact was made "viewed"';
        }

        $em->persist($contact);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => $message
        ]);
    }

    /**
     * Delete record
     * @Route("/admin/contact/delete/{contact}", name="contact_delete")
     * @param Contact $contact
     * @return JsonResponse
     */
    public function delete(Contact $contact)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($contact);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => "Contact was delete successfully"
        ]);
    }
}
