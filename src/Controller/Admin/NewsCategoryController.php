<?php

namespace App\Controller\Admin;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use App\Entity\Categories;
use App\Utils\Slugger;
use App\Form\CategoriesType;
use App\Repository\CategoriesRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NewsCategoryController extends AbstractController
{
    /**
     * Show categories list
     * @Route("/admin/category", name="category_index")
     */
    public function index()
    {
        return $this->render('admin/news_category/index.html.twig', ['activePage' => 'categories']);
    }

    /**
     * Return JSON with categories list for data table plugin
     * @Route("/admin/category-list", name="category_list")
     * @param Request $request
     * @param CategoriesRepository $repository
     * @return Response
     * @throws ExceptionInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function categoryList(Request $request, CategoriesRepository $repository)
    {
        $columns = [
            0 => 'name',
            1 => 'create_at',
        ];

        //Config search data
        $search = $request->get('search');

        if (!empty($search['value'])) {
            $params['search'] = $search['value'];
        }

        //Config order
        $orderConfig = $request->get('order');
        $params['order_column'] = $columns[$orderConfig[0]['column']];
        $params['order_dir'] = $orderConfig[0]['dir'];

        //Config pagination
        $params['limit'] = $request->get('length');
        $params['offset'] = $request->get('start');

        $categories = $repository->getDtData($params);

        $data = [];

        if (!empty($categories)) {
            foreach ($categories as $key => $item) {
                $serializer = new Serializer(array(new DateTimeNormalizer()));
                $dateAsString = $serializer->normalize($item->getCreateAt());

                $data[$key] = [
                    'name'     => $item->getName(),
                    'date_add' => date('d.m.Y', strtotime($dateAsString)),
                    'action'   => $this->renderView('admin/news_category/partial/action.html.twig', ['id' => $item->getId()])
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $repository->countAll();
        $output['recordsFiltered'] = count($categories);

        $response = new JsonResponse($output);

        return $response;
    }

    /**
     * Show create form
     * @Route("/admin/category/form", name="category_create")
     */
    public function create()
    {
        $categories = new Categories();

        $form = $this->createForm(CategoriesType::class, $categories, [
            'action' => $this->generateUrl('category_store'),
            'method' => 'POST'
        ]);

        return $this->render('admin/news_category/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Show edit form
     * @Route("/admin/category/form/{category}", name="category_edit")
     * @param Categories $category
     * @return Response
     */
    public function edit(Categories $category)
    {
        $form = $this->createForm(CategoriesType::class, $category, [
            'action' => $this->generateUrl('category_update', ['category' => $category->getId()]),
            'method' => 'POST'
        ]);

        return $this->render('admin/news_category/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Store record
     * @Route("/admin/category/store", name="category_store")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $category = new Categories();

        $form = $this->createForm(CategoriesType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setCreateAt(new \DateTime('@'.strtotime('now')));
            $category->setAlias(Slugger::slugify($category->getName()));

            $em->persist($category);
            $em->flush();
        }

        return new JsonResponse([
            'status'  => 'success',
            'message' => "Category was added"
        ]);
    }

    /**
     * Update record
     * @Route("/admin/category/update/{category}", name="category_update")
     * @param Request $request
     * @param Categories $category
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, Categories $category)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CategoriesType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setAlias(Slugger::slugify($category->getName()));

            $em->persist($category);
            $em->flush();
        }

        return new JsonResponse([
            'status'  => 'success',
            'message' => "Category was updated"
        ]);
    }

    /**
     * Delete record
     * @Route("/admin/category/delete/{category}", name="category_delete")
     * @param Categories $category
     * @return JsonResponse
     */
    public function delete(Categories $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return new JsonResponse([
            'status'  => 'success',
            'message' => "Category was delete successfully"
        ]);
    }
}
