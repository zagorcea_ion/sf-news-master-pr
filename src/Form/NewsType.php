<?php

namespace App\Form;

use App\Entity\Categories;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('id_category', EntityType::class, [
                'label'        => 'Category',
                'class'        => Categories::class,
                'choice_label' => 'name',
            ])
            ->add('content', TextareaType::class, ['required' => false])
            ->add('image', FileType::class, ['mapped' => false])
            ->add('date_publish', DateType::class, ['widget' => 'single_text'])
            ->add('is_published', CheckboxType::class, [
                'required' => false,
                'attr'     => ['id' => 'is_published']
                ])
            ->add('is_must_read', CheckboxType::class, [
                'required' => false,
                'attr'     => ['id' => 'must-read']
                ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary', 'label' => 'Submit'],
            ])
        ;
    }
}
