<?php


namespace App\Utils;


use Symfony\Component\Filesystem\Filesystem;

class FilesHelper
{
    /**
     * Delete file
     * @param string $path
     * @param null $fileName
     * @return boolean
     */
    public static function deleteFile(string $path, $fileName = null)
    {
        if (!is_null($fileName)) {
            $filesystem = new Filesystem();

            $filePath = $path . '/' . $fileName;

            if ($filesystem->exists($filePath)) {
                return unlink($filePath);
            }
        }
        return false;
    }

    /**
     * Save file
     * @param string $path
     * @param $file
     * @return string
     */
    public static function saveFile(string $path, $file)
    {
        $fileName = uniqid() . '.' . $file->guessExtension();
        $file->move($path, $fileName);

        return $fileName;
    }

    /**
     * Save base64 as image file
     * @param string $path
     * @param $base64Image
     * @return string
     */
    public static function saveBase64Image(string $path, $base64Image)
    {
        $image = str_replace('data:image/jpeg;base64,', '', $base64Image);
        $image = str_replace(' ', '+', $image);

        $imageName = uniqid() . '.png';

        file_put_contents($path . '/' . $imageName, base64_decode($image));

        return $imageName;
    }

}