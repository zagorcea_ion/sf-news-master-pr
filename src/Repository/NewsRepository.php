<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * Count all records
     * @param array $params
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAll($params = [])
    {
        $qb = $this->createQueryBuilder('n');
        $qb->select('count(n.id)');

        if (!empty($params['search'])) {
            $qb->where('n.title like :title')
                ->setParameter('title', '%' . $params['search'] . '%');
        }

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Select all record for data table list
     * @param array $params
     * @return mixed
     */
    public function getDtData($params = [])
    {
        $qb = $this->createQueryBuilder('n');

        if (!empty($params['search'])) {
            $qb->where('n.title like :title')
                ->setParameter('title', '%' . $params['search'] . '%');
        }

        if (!empty($params['order_column']) && !empty($params['order_dir'])) {
            $qb->addOrderBy('n.' . $params['order_column'], $params['order_dir']);
        }

        if (!empty($params['limit']) && !empty($params['offset'])) {
            $qb->setMaxResults($params['limit'])
                ->setFirstResult($params['offset']);

        } else if (!empty($params['limit'])) {
            $qb->setMaxResults($params['limit']);
        }

        return $qb->getQuery()->getResult();

    }

    /**
     * Select record with status must read
     * @param int $limit
     * @return mixed
     */
    public function getIsMustRead($limit = 6)
    {
        $qb = $this->createQueryBuilder('n');

        $qb->where('n.is_published = 1')
            ->andWhere('n.is_must_read = 1')
            ->addOrderBy('n.date_publish', 'DESC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * Select last published records
     * @param null $category
     * @return mixed
     */
    public function getLast($category = null)
    {
        $qb = $this->createQueryBuilder('n');

        $qb->where('n.is_published = 1');

        if (!is_null($category)) {
            $qb->andWhere('n.id_category = ' . $category);
        }

        $qb->addOrderBy('n.date_publish', 'DESC');

        return $qb->getQuery()->getResult();

    }

    /**
     * Select most popular records
     * @param int $limit
     * @return mixed
     */
    public function getMostPopular($limit = 6)
    {
        $qb = $this->createQueryBuilder('n');

        $qb->where('n.is_published = 1')
            ->addOrderBy('n.views', 'DESC')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();

    }

    /**
     * Select random records
     * @param int $limit
     * @param null $exceptionId
     * @throws DBALException
     * @return mixed
     */
    public function getRandom($limit = 3, $exceptionId = null)
    {
        $conn = $this->getEntityManager()
            ->getConnection();

        $sql = 'SELECT * FROM news ';

        if (!is_null($exceptionId)) {
            $sql .= 'where id != ' . $exceptionId;
        }

        $sql .= ' ORDER BY RAND() LIMIT ' . $limit;

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
