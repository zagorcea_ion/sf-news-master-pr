<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    /**
     * Count all records
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @return mixed
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('c');

        return $qb->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Select all record for data table list
     * @param array $params
     * @return mixed
     */
    public function getDtData($params = [])
    {
        $qb = $this->createQueryBuilder('c');


        if (!empty($params['order_column']) && !empty($params['order_dir'])) {
            $qb->addOrderBy('c.' . $params['order_column'], $params['order_dir']);
        }

        if (!empty($params['limit']) && !empty($params['offset'])) {
            $qb->setMaxResults($params['limit'])
                ->setFirstResult($params['offset']);
        }

        return $qb->getQuery()->getResult();

    }
}
